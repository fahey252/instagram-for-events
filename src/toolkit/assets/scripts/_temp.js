/*!
 * Mass Relevance global vars and functions JS file
 *
 * Developer(s): Kevin Mack
 *
 * * * Limitations and Future Updates: Should cache JSON, use timestamp to prevent multiple calls,
 * * * more logic if localstorage is not supported, pagination for progressive enhancement, verify image URLs
 *
 */

window.MR = {};

var windowHeight = $(window).height();
var moveHeight = 490;

var delayOnPageMove = 8000;
var callTimeout;

// Number of posts to return
var numberOfPosts = 50;

var reverse = ( oldToNew ) ? "" : "reverse=1&"; // Set in footer.php

var $container = $("#content");

//var baseURL = "http://tweetriver.com/tobius/resource.json?reverse=1"; // Set in "footer.php"

var localStorageSupported = ( typeof (Storage) !== "undefined" ) ? true : false; // Check to see if localstorage is supported


// Verify image and hide if it errors (404, etc)
MR.verifyImage = function (image) {
  image.onerror = "";
  image.src = "";
  $(image).parent().hide(); // Hide parent `.box`
  return true;
};

// Convert epoch to "days ago" for posts
MR.epochConvert = function (time, errorMsg) {

  if ( typeof errorMsg == "undefined" ) errorMsg = " -- "; // This string is returned if `time` is not a number, null, or undefined. Override available as argument option.

  if ( time == null || typeof time != "number" ) return errorMsg; // if time is not passed through, don't process any more.

  if ( time > 999999999999 ) time /= 1000; // Convert from "timestamp in milliseconds"

  var typeStr, now = new Date().valueOf() / 1000,
    diff = now - time;
  var minutes = Math.floor( diff / 60 );
  var hours = Math.floor(  diff / ( 60 * 60 )  );
  var days = Math.floor(  diff / ( 24 * 60 * 60 )  );

  if ( days > 365 ) { // Over a year ago
    return "Over a year ago";
  }
  if ( days >= 1 ) { // X days ago
    typeStr = "day";
    time = days;
  }
  if ( days <= 0 ) { // X hours ago
    typeStr = "hour";
    time = hours;
  }
  if ( hours <= 0 ) { // X minutes ago
    typeStr = "minute";
    time = minutes;
  }
  if ( minutes <= 0 ) { // X seconds ago
    typeStr = "second";
    time = Math.floor( diff / 60 * 60 );
  }

  output = time + " " + typeStr;

  if (time > 1) output += "s"; // Fix for "1 days", "1 hours", etc.

  return output += " ago";

};

// Make strings to URL for posts
MR.urlify = function (message, network) {

  var network = ( _.isUndefined(network) ) ? "twitter" : network;
  message = ( _.isUndefined(message) ) ? "" : message;

  var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/i;
  message = message.replace(exp, "<a href='$1' target='_blank'>$1</a>");

  if (network == "twitter") {
    exp = /(^|\s)#(\w+)/g;
    message = message.replace(exp, "$1<a href='https://twitter.com/search?q=%23$2' target='_blank'>#$2</a>");
    exp = /(^|\s)@(\w+)/g;
    message = message.replace(exp, "$1<a href='http://www.twitter.com/$2' target='_blank'>@$2</a>");
  }

  return message;

};


var successfulDataPull = function (data) {

  var timestamp = new Date().valueOf(); // Epoch time

  /*
   * When initial data is successful pulled from "baseURL"
   * Adds the data to the page using underscore template
   * Creates the instance of isotope to fit content in the screen pretty
   * Creates the instance of infinite scroll (plugin)
   */
  $("#content").append(
    _.template($("#content-aggregator").html(), {
        data: data // from ready() ajax call
    })
  );

  // Isotope plugin initialize
  $container.imagesLoaded(function () { // load images first
      $container.isotope({
          itemSelector: ".box",
          masonry: {
            columnWidth: 490
          },
          cellsByRow: {
            columnWidth: 490,
            rowHeight: 490
          }
        });
    });

  // Infinite Scroll plugin initialize
  $container.infinitescroll({
      appendCallback: false,
      behavior: ( Modernizr.touch ) ? "manual" : undefined,
      bufferPx: 600,
      dataType: "json",
      itemSelector: ".box",
      navSelector: "#nav > a",
      nextSelector: "#nav > a",
      path: [baseURL + "?" + reverse + "limit=" + numberOfPosts + "&start_id=" + MR.sinceId], // Needed for JSON with infinite scroll

      loading: {
        msgText: "<p>Loading More</p>",
        finishedMsg: 'No more content to load.',
        img: 'http://i.imgur.com/qkKy8.gif'
      },

      // debug: true, // TESTING ONLY
      // localMode    : true, // TESTING ONLY

    }, function (json, opts) {

      if( json.length > 0 ) {

        MR.sinceId = json[json.length - 1].entity_id;
        // Twitter Fix for MR
        if ( typeof MR.sinceId === "undefined" ) MR.sinceId = json[json.length - 1].id_str;
        /*
         * "page" parameter needed to prevent caching of URL, keeps it unique each time.
         * Update "limit" to change # of posts that are received per pull
         */

      }

      url = ( json.length == 0 || json.length < ( numberOfPosts - 2 ) ) ? ( baseURL + "?" + reverse + "limit=" + numberOfPosts ) : ( baseURL + "?" + reverse + "limit=" + numberOfPosts + "&start_id=" + MR.sinceId );

      // Updated JSON from Mass Relevance
      var dom = _.template($("#content-aggregator").html(), {
          data: json
      });

      var $newPosts = $(dom).css({
          opacity: 0
      });

      $newPosts.imagesLoaded(function () {
          $container.isotope("insert", $newPosts, true);
          // jQuery(".box p").fitText(1.2);
      });

      $container.data("infinitescroll").options.path = [url];


    });

};

/**
 * Auto Scrolls page
 */
var autoScroll = function () {

  _.delay(function () {
      $("body").scrollTo(  ( "+=" + moveHeight + "px" ), ( delayOnPageMove + delayOnPageMove )  );
  }, 1500);

};

/**
 * Check For Newer posts
 * Pulls the latest feed appends at the end
 */
var checkForNew = function () {
  setInterval(function () {
      $.ajax({
          url: baseURL + "?limit=5",
          dataType: "json",
          success: function (json) {

            // Create a temp var to compare to the "newest post"
            var tempNewId = ( oldToNew ) ? json[0].since_id : json[json.length - 1].since_id;
            // Twitter fix from MR feed:
            if ( typeof tempNewId === "undefined" ) tempNewId = ( oldToNew ) ? json[0].entity_id : json[json.length - 1].entity_id;

            if ( tempNewId !== MR.newestID ) {
              // Assign the "newest ID"
              MR.newestID = tempNewId;
              // Update sinceID
              MR.sinceId = json[0].entity_id;
              // Twitter Fix for MR
              if (typeof MR.sinceId === "undefined") MR.sinceId = json[json.length - 1].id_str;
              //Update URL
              $container.data("infinitescroll").options.path = [baseURL + "?" + reverse + "limit=" + numberOfPosts];
            }

          }
      });
  }, 90000);

}

$(function () {

    var containerWidth = $("#main").width();
    var gridWidth = 490;
    var divisibleByGridWidth = Math.floor( $("#main").width() / gridWidth );

    // Center container based off of width of window
    var contentMargins = ( containerWidth - 2 - ( divisibleByGridWidth * gridWidth ) ) / 2;

    if (contentMargins < 0) contentMargins = 0;
    $("#content").css({
        "margin-left": contentMargins,
        "margin-right": contentMargins
    });


    // Call Mass Relevance Data
    $.ajax({
        url: baseURL + "?" + reverse + "limit=" + numberOfPosts,
        dataType: "json",
        success: function (data) {

          successfulDataPull(data);

          // window.json = data; // TESTING ONLY

          // Create global variable for id
          MR.sinceId = data[data.length - 1].since_id;
          MR.newestID = data[0].since_id;

          /**
           * Twitter Fix for MR
           * since_id: Includes only those status entities approved after supplied status entity_id biasing towards real-time. *Note*: id_str should be used when linking to a Tweet.
           */
          if (typeof MR.sinceId === "undefined") MR.sinceId = data[data.length - 1].entity_id;
          if (typeof MR.newestID === "undefined") MR.newestID = data[0].entity_id;
          if (oldToNew) MR.newestID = MR.sinceId;

          // Update the url in the ajax call for infinite scroll
          $container.data("infinitescroll").options.path = [baseURL + "?" + reverse + "limit=" + numberOfPosts + "&start_id=" + MR.sinceId];

        }
    });

    /**
     * Auto Scroll page
     * Turn off autoscroll by putting "autoscrollOff" in the url, example: http://mr.rlab.co?autoscrollOff
     */
    if (autoscroll) setInterval(autoScroll, 1500 );

    // Auto Check For Newest Tweets
    checkForNew();

});


var resizeTimeout;
$(window).on("resize", function() {
  clearTimeout(resizeTimeout);
  resizeTimeout = setTimeout(function() {
    var href = location.href;
    document.location.href = href;
    // location.reload(true);
    //location.href = ""
  }, 1000);
});
